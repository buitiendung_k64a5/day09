<?php
session_start();
$keys = array("q0" => 1, "q1" => 4, "q2" => 2, "q3" => 3, "q4" => 1, "q5" => 1, "q6" => 4, "q7" => 2, "q8" => 3, "q9" => 1);
$point = 0;
$msg = "Bạn quá kém, cần ôn tập thêm.";

foreach (array_keys($keys) as $k) {
    if ($keys[$k] == $_COOKIE[$k]) {
        ++$point;
    }
}

if ($point > 3 && $point < 8) {
    $msg = "Cũng bình thường";
}
else if ($point > 7) {
    $msg = "Sắp sửa làm được trợ giảng lớp PHP";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="index.css">
</head>
<body>
    <?php echo "<form><h3>Điểm của bạn: $point.</h3><h3>Nhận xét: $msg</h3></form>" ?>
</body>
</html>